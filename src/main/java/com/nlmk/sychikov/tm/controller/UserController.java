package com.nlmk.sychikov.tm.controller;

import com.nlmk.sychikov.tm.entity.User;
import com.nlmk.sychikov.tm.enumerated.RoleType;
import com.nlmk.sychikov.tm.service.UserService;

import java.util.List;

public class UserController extends AbstractController {

    private final UserService userService;

    private User currentUser;

    public UserController(final UserService userService) {
        this.userService = userService;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public Long getCurrentUserId() {
        return currentUser.getId();
    }

    public String getCurrentLogin() {
        if (!isAuthenticated()) return null;
        return currentUser.getLogin();
    }

    public RoleType getCurrentRole() {
        if (!isAuthenticated()) return null;
        return currentUser.getRoleType();
    }

    /**
     * Check does the user have ADMIN role or user is current user
     * and print alarm if hasn't.
     *
     * @return true if has ADMIN role or user is current user.
     */
    private boolean checkUserPermission(final User user) {
        if (user.getId().equals(getCurrentUserId())) return true;
        return checkCurrentUserAdminPermissionAlarm();
    }

    /**
     * Check does the current user have ADMIN role
     *
     * @return true if has ADMIN role.
     */
    public boolean checkCurrentUserAdminPermissionAlarm() {
        final boolean result = checkCurrentUserAdminPermission();
        if (!result) System.out.println("[You don't have permission]");
        return result;
    }

    /**
     * Check does the current user have ADMIN role
     * and print alarm if hasn't.
     *
     * @return true if has ADMIN role.
     */
    public boolean checkCurrentUserAdminPermission() {
        final RoleType roleType = getCurrentRole();
        if (roleType == null) return false;
        return roleType.equals(RoleType.ADMIN);
    }

    /**
     * Check current authentication.
     *
     * @return true if authenticated.
     */
    public boolean isAuthenticated() {
        return !(currentUser == null);
    }

    /**
     * Clear all users
     *
     * @return return value
     */
    public int clearUsers() {
        System.out.println("[CLEAR USERS]");
        if (checkCurrentUserAdminPermissionAlarm()) {
            userService.clear();
            System.out.println("[OK]");
            return 0;
        }
        return -1;
    }

    /**
     * Show the user
     *
     * @param user what user should be printed
     */
    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME :" + user.getFirstName());
        System.out.println("MIDDLE NAME :" + user.getMiddleName());
        System.out.println("LAST NAME :" + user.getLastName());
        System.out.println("ROLE :" + user.getRoleType());
        System.out.println("[OK]");
    }

    /**
     * Shows user by index
     *
     * @return return value
     */
    public int viewUserByIndex() {
        final int index = intInputProcessor("Enter user index: ");
        if (index == -1)
            return 0;
        final User user = userService.findByIndex(index);
        if (user == null)
            System.out.println("User with index=" + (index + 1) + " is not found!");
        else viewUser(user);
        return 0;
    }

    /**
     * Shows user by id
     *
     * @return return value
     */
    public int viewUserById() {
        final Long id = longInputProcessor("Enter user id: ");
        if (id == -1)
            return -1;
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("User with index=" + id.toString() + " is not found!");
            return -1;
        }
        if (checkUserPermission(user)) {
            viewUser(user);
            return 0;
        } else
            return -1;
    }

    /**
     * Shows user by login
     *
     * @return return value
     */
    public int viewUserByLogin() {
        System.out.print("Enter user login: ");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("User '" + login + "' is not found!");
            return -1;
        }
        if (checkUserPermission(user)) {
            viewUser(user);
            return 0;
        } else
            return -1;
    }

    /**
     * Removes user by login
     *
     * @return return value
     */
    public int removeUserByLogin() {
        System.out.println("[REMOVE USER]");
        if (!checkCurrentUserAdminPermissionAlarm()) {
            return -1;
        }
        System.out.print("Enter user login: ");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("User with login=" + login + " is not found!");
            System.out.println("[FAILED]");
            return -1;
        }
        userService.remove(user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Removes user by id
     *
     * @return return value
     */
    public int removeUserById() {
        System.out.println("[REMOVE USER]");
        if (!checkCurrentUserAdminPermissionAlarm()) {
            return -1;
        }
        final Long id = longInputProcessor("Enter user id: ");
        if (id == -1)
            return -1;
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("User with id=" + id.toString() + " is not found!");
            System.out.println("[FAILED]");
            return -1;
        }
        userService.remove(user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Updates user by "user" object
     */
    private void updateUser(final User user) {
        System.out.print("Enter first name[" + user.getFirstName() + "]: ");
        String firstName = scanner.nextLine();
        if (firstName == null || firstName.isEmpty()) firstName = user.getFirstName();
        System.out.print("Enter middle name[" + user.getMiddleName() + "]: ");
        String middleName = scanner.nextLine();
        if (middleName == null || middleName.isEmpty()) middleName = user.getMiddleName();
        System.out.print("Enter last name[" + user.getLastName() + "]: ");
        String lastName = scanner.nextLine();
        if (lastName == null || lastName.isEmpty()) lastName = user.getLastName();
        System.out.print("Enter role type[" + user.getRoleType().name() + "]: ");
        String roleType = scanner.nextLine();
        if (roleType == null || roleType.isEmpty())
            userService.update(user, firstName, middleName, lastName, user.getRoleType());
        try {
            userService.update(user, firstName, middleName, lastName, RoleType.valueOf(roleType));
        } catch (IllegalArgumentException e) {
            System.out.println("Wrong role type! Role type does not changed.");
            userService.update(user, firstName, middleName, lastName, user.getRoleType());
        }
    }

    /**
     * Updates user by id
     *
     * @return return value
     */
    public int updateUserById() {
        System.out.println("[UPDATE USER]");
        final Long id = longInputProcessor("Enter user id: ");
        if (id == -1)
            return -1;
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("User with id=" + id.toString() + " is not found!");
            System.out.println("[FAILED]");
            return -1;
        }
        if (!checkUserPermission(user)) return -1;
        updateUser(user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Updates user by login
     *
     * @return return value
     */
    public int updateUserByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.print("Enter user login: ");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("User with login=" + login + " is not found!");
            System.out.println("[FAILED]");
            return -1;
        }
        if (!checkUserPermission(user)) return -1;
        updateUser(user);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Render user from list
     *
     * @param users user list
     */
    private void renderUsers(final List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (User user : users) {
            System.out.println(index + ". " + user.toString());
            index++;
        }
    }

    /**
     * List all users
     *
     * @return return value
     */
    public int viewAllUsers() {
        System.out.println("[LIST USERS]");
        if (!checkCurrentUserAdminPermissionAlarm()) return -1;
        renderUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Add new user
     *
     * @return return value
     */
    public int addUser() {
        System.out.println("[ADD NEW USER]");
        if (!checkCurrentUserAdminPermissionAlarm()) return -1;
        System.out.print("Enter new login: ");
        final String login = scanner.nextLine();
        if (userService.findByLogin(login) != null) {
            System.out.println("Login " + login + " already exists!");
            System.out.println("[FAILED]");
            return -1;
        }
        final String password = enterNewPassword();
        if (password == null || password.isEmpty()) {
            System.out.println("[FAILED]");
            return -1;
        }
        System.out.print("Enter first name[]: ");
        final String firstName = scanner.nextLine();
        System.out.print("Enter middle name[]: ");
        final String middleName = scanner.nextLine();
        System.out.print("Enter last name[]: ");
        final String lastName = scanner.nextLine();
        final User user = userService.addUser(login, password, firstName, middleName, lastName);
        if (user == null) {
            System.out.println("[FAILED]");
            return -1;
        }
        System.out.println("[OK]");
        return 0;
    }

    /**
     * New password input dialog
     *
     * @return checked password or null
     */
    private String enterNewPassword() {
        System.out.print("Enter new password: ");
        final String password = scanner.nextLine();
        System.out.print("Confirm password: ");
        final String confirmedPassword = scanner.nextLine();
        final Boolean confirmed = checkPassword(password, confirmedPassword);
        if (!confirmed) {
            System.out.println("Passwords don't match or do not correspond the password agreement!");
            return null;
        }
        return password;
    }

    /**
     * Check passwords agreement and matching
     *
     * @param password          first password input
     * @param confirmedPassword second (repeated) password input
     * @return true if passwords correspond to the rule
     */
    private Boolean checkPassword(String password, String confirmedPassword) {
        return password.equals(confirmedPassword);
    }

    /**
     * User authentication method
     *
     * @return return value
     */
    public int login() {
        System.out.print("login: ");
        final String login = scanner.nextLine();
        if (login == null || login.isEmpty()) return -1;
        System.out.print("password: ");
        final String password = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) return -1;
        if (userService.checkPasswordByLogin(login, password)) {
            currentUser = user;
        }
        return 0;
    }

    /**
     * Finish user session
     *
     * @return return value
     */
    public int logout() {
        System.out.println("[" + getCurrentLogin() + " now logging out]");
        currentUser = null;
        return 0;
    }

    /**
     * Shows current user profile
     *
     * @return return value
     */
    public int viewCurrentProfile() {
        viewUser(currentUser);
        return 0;
    }

    /**
     * Update current user profile
     *
     * @return return value
     */
    public int updateCurrentProfile() {
        System.out.println("[UPDATE CURRENT PROFILE]");
        if (!isAuthenticated()) {
            System.out.println("[FAILED]");
            return -1;
        }
        updateUser(currentUser);
        return 0;
    }

    /**
     * Change current user password
     *
     * @return return value
     */
    public int changeCurrentPassword() {
        final String password = enterNewPassword();
        if (password == null || password.isEmpty()) return -1;
        userService.setPasswordById(getCurrentUserId(), password);
        System.out.println("[Password was changed]");
        return 0;
    }

    /**
     * Change user password
     *
     * @return return value
     */
    public int changePassword() {
        if (!checkCurrentUserAdminPermissionAlarm()) return -1;
        System.out.print("login: ");
        final String login = scanner.nextLine();
        if (login == null || login.isEmpty()) return -1;
        final User user = userService.findByLogin(login);
        if (user == null) return -1;
        final String password = enterNewPassword();
        if (password == null || password.isEmpty()) return -1;
        userService.setPasswordById(user.getId(), password);
        System.out.println("[Password was changed]");
        return 0;
    }

}
