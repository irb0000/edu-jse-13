package com.nlmk.sychikov.tm.enumerated;

public enum RoleType {

    USER("User"),
    ADMIN("Administrator");

    private final String displayName;

    RoleType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
