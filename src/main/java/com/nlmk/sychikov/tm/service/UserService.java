package com.nlmk.sychikov.tm.service;

import com.nlmk.sychikov.tm.entity.User;
import com.nlmk.sychikov.tm.enumerated.RoleType;
import com.nlmk.sychikov.tm.repository.UserRepository;
import com.nlmk.sychikov.tm.util.HashUtil;

import java.util.List;

public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User addUser(
            final String login, final String password, final String firstName, final String middleName,
            final String lastName, RoleType roleType
    ) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        final String passwordHash = HashUtil.encryptMd5(password);
        if (firstName == null || firstName.isEmpty())
            return userRepository.addUser(login, passwordHash);
        if (middleName == null || middleName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (userRepository.findByLogin(login) != null) return null;
        return userRepository.addUser(login, passwordHash, firstName, middleName, lastName, roleType);
    }

    public User addUser(
            final String login, final String password, final String firstName, final String middleName,
            final String lastName
    ) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        final String passwordHash = HashUtil.encryptMd5(password);
        if (firstName == null || firstName.isEmpty())
            return userRepository.addUser(login, passwordHash);
        if (middleName == null || middleName.isEmpty()) return null;
        if (lastName == null || lastName.isEmpty()) return null;
        if (userRepository.findByLogin(login) != null) return null;
        return userRepository.addUser(login, passwordHash, firstName, middleName, lastName);
    }

    public User addUser(final String login, final String password, RoleType roleType) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        final String passwordHash = HashUtil.encryptMd5(password);
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.addUser(login, passwordHash, roleType);
    }

    public User addUser(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        final String passwordHash = HashUtil.encryptMd5(password);
        if (passwordHash == null || passwordHash.isEmpty()) return null;
        return userRepository.addUser(login, passwordHash);
    }

    public void clear() {
        userRepository.clear();
    }

    public User findByIndex(final int index) {
        if (index > userRepository.getRepositorySize() - 1 || index < 0) return null;
        return userRepository.findByIndex(index);
    }

    public User findById(final Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public User remove(final User user) {
        if (user == null) return null;
        return userRepository.remove(user);
    }

    public User update(
            final User user, final String firstName, final String middleName,
            final String lastName
    ) {
        if (user == null) return null;
        return userRepository.update(user, firstName, middleName, lastName);
    }

    public User update(
            final User user, final String firstName, final String middleName,
            final String lastName, final RoleType roleType
    ) {
        if (user == null) return null;
        return userRepository.update(user, firstName, middleName, lastName, roleType);
    }

    public User removeByIndex(final int index) {
        final User user = findByIndex(index);
        return remove(user);
    }

    public User removeById(final Long id) {
        final User user = findById(id);
        return remove(user);
    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        return remove(user);
    }

    private User setPassword(final User user, final String password) {
        if (user == null) return null;
        if (password == null) return null;
        return userRepository.setPasswordHash(user, HashUtil.encryptMd5(password));
    }

    public User setPasswordById(final Long id, final String password) {
        if (id == null) return null;
        if (password == null || password.isEmpty()) return null;
        final User user = findById(id);
        return setPassword(user, password);
    }

    public boolean checkPasswordByLogin(final String login, final String password) {
        final String passwordHash = HashUtil.encryptMd5(password);
        final User user = findByLogin(login);
        if (user == null) return false;
        return user.getPasswordHash().equals(passwordHash);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
